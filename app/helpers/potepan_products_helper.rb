module PotepanProductsHelper
  def back_path(path, &block)
    if request.referer&.include?(path)
      link_to capture(&block), request.referer
    end
  end
end
