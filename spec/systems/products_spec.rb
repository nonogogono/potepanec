require 'rails_helper'

RSpec.describe "Products", type: :system do
  describe "show layout" do
    let!(:product) { create(:product) }
    let!(:image) { File.open(File.expand_path('../fixtures/thinking-cat.jpg', __dir__)) }
    let!(:product_image) { product.images.create!(attachment: image) }

    it "商品詳細ページにアクセスする" do
      visit potepan_product_path(product.id)

      within ".navbar" do
        expect(page).to have_link nil, href: potepan_path, count: 2
      end

      within first(".media-left") do
        expect(page).to have_xpath("//img[contains(@src,'thinking-cat')]")
      end

      within first(".media-body") do
        expect(page).not_to have_link "一覧ページへ戻る"
        expect(page).to have_content product.name
        expect(page).to have_content product.display_amount
        expect(page).to have_content product.description
      end
    end
  end
end
