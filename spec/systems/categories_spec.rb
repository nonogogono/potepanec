require 'rails_helper'

RSpec.describe "Categories", type: :system do
  describe "show layout" do
    let!(:categories_taxonomy) { create(:taxonomy, name: "Categories") }
    let!(:brand_taxonomy) { create(:taxonomy, name: "Brand") }
    let!(:categories_taxon) { categories_taxonomy.taxons.first }
    let!(:brand_taxon) { brand_taxonomy.taxons.first }
    let!(:bags_taxon) { create(:taxon, name: "Bags", taxonomy_id: categories_taxonomy.id, parent_id: categories_taxon.id) }
    let!(:clothing_taxon) { create(:taxon, name: "Clothing", taxonomy_id: categories_taxonomy.id, parent_id: categories_taxon.id) }
    let!(:tshirts_taxon) { create(:taxon, name: "T-Shirts", taxonomy_id: categories_taxonomy.id, parent_id: clothing_taxon.id) }
    let!(:ruby_taxon) { create(:taxon, name: "Ruby", taxonomy_id: brand_taxonomy.id, parent_id: brand_taxon.id) }
    let!(:rails_taxon) { create(:taxon, name: "Rails", taxonomy_id: brand_taxonomy.id, parent_id: brand_taxon.id) }
    let!(:bags_rails_product_1) { create(:product, name: "Ruby on Rails Tote", price: 15.99, taxons: [bags_taxon, rails_taxon]) }
    let!(:bags_rails_product_2) { create(:product, name: "Ruby on Rails Bag", price: 22.99, taxons: [bags_taxon, rails_taxon]) }
    let!(:tshirts_rails_product_1) { create(:product, name: "Ruby on Rails Baseball Jersey", price: 19.99, taxons: [tshirts_taxon, rails_taxon]) }
    let!(:tshirts_rails_product_2) { create(:product, name: "Ruby on Rails Ringer T-Shirt", price: 19.99, taxons: [tshirts_taxon, rails_taxon]) }
    let!(:tshirts_ruby_product) { create(:product, name: "Ruby Baseball Jersey", price: 19.99, taxons: [tshirts_taxon, ruby_taxon]) }
    let!(:image) { File.open(File.expand_path('../fixtures/thinking-cat.jpg', __dir__)) }
    let!(:bags_rails_product_1_image) { bags_rails_product_1.images.create!(attachment: image) }

    before do
      visit potepan_category_path(bags_taxon.id)
    end

    it "商品一覧ページにアクセスする" do
      within ".navbar" do
        expect(page).to have_link nil, href: potepan_path, count: 2
      end

      within ".taxonomy-body" do
        expect(page).to have_content categories_taxonomy.name
        expect(page).to have_content brand_taxonomy.name
      end

      within "#taxonomy-0" do
        expect(page).not_to have_content categories_taxon.name
        expect(page).to have_content bags_taxon.name
        expect(page).not_to have_content clothing_taxon.name
        expect(page).to have_content tshirts_taxon.name
      end

      within "#taxonomy-1" do
        expect(page).not_to have_content brand_taxon.name
        expect(page).to have_content ruby_taxon.name
        expect(page).to have_content rails_taxon.name
        expect(page).to have_content rails_taxon.all_products.count
        expect(rails_taxon.all_products.count).to eq 4
      end

      within ".products-area" do
        expect(page).to have_xpath("//img[contains(@src,'thinking-cat')]")
        expect(page).to have_content bags_rails_product_1.name
        expect(page).to have_content bags_rails_product_1.display_amount
        expect(page).not_to have_content tshirts_rails_product_1.name
        expect(page).not_to have_content tshirts_rails_product_1.display_amount
        expect(page).to have_selector(".productBox", count: bags_taxon.all_products.count)
      end
    end

    it "商品一覧ページ → 商品詳細ページから「一覧ページへ戻る」をクリックする" do
      click_link bags_rails_product_1.name
      expect(current_path).to eq potepan_product_path(bags_rails_product_1.id)
      expect(page).to have_link "一覧ページへ戻る"
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(bags_taxon.id)
    end
  end
end
