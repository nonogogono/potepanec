require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET products#show" do
    let!(:product) { create(:product) }
    let!(:image) { File.open(File.expand_path('../fixtures/thinking-cat.jpg', __dir__)) }
    let!(:product_image) { product.images.create!(attachment: image) }

    before do
      get potepan_product_path(product.id)
    end

    it "リクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "product の情報が表示されていること" do
      expect(response.body).to include "thinking-cat.jpg"
      expect(response.body).to include product.name
      expect(response.body).to include product.display_amount.to_s
      expect(response.body).to include product.description
    end
  end
end
