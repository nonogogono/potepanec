require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET categories#show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:image) { File.open(File.expand_path('../fixtures/thinking-cat.jpg', __dir__)) }
    let!(:product_image) { product.images.create!(attachment: image) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "リクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "taxon に基づいた product の情報が表示されていること" do
      expect(response.body).to include "thinking-cat.jpg"
      expect(response.body).to include product.name
      expect(response.body).to include product.display_amount.to_s
    end
  end
end
